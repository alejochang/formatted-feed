## Feed formatter
@alejochang

###feed-helper
It's a basic node module that formats a readable text from likes given to blog posts. 
It expects an array of blog posts, where post is being described as:
```
{
    id: number,
    likes: string[]

}
```
Likes are the names of people that have liked a given post.
If a blog post hasn't received any likes, an empty array is expected.


To run it, once located in the path, run the example node app:
`node feed-app.js`

###Example app:
Passes sample data (input):

```
const posts = [
  { id: 1, likes: [] },
  { id: 2, likes: ['Peter'] },
  { id: 3, likes: ['John', 'Mark'] },
  { id: 4, likes: ['Paul', 'Lilly', 'Alex'] },
  { id: 5, likes: ['Sarah', 'Michelle', 'Alex', 'John'] }
];
```
Imports `feed-helper`, and passes the sample data to `formatTextFromLikes`
Prints the result in the console log, and checks if the result is the same as the expected one.

```
const expectedOutput = [
  { id: 1, text: 'No one likes this' },
  { id: 2, text: 'Peter likes this' },
  { id: 3, text: 'John and Mark like this' },
  { id: 4, text: 'Paul, Lilly and Alex like this' },
  { id: 5, text: 'Sarah, Michelle and 2 others like this' }
];
```
