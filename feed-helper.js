'use strict';

const formatTextFromLikes = (posts) => {
    return posts.map(post =>  {
        return {
            id: post.id,
            text: namesFormatter(post.likes)
        }}
    )
  }

  const likeThis = (names) => {
    let likeText = ' like';
    if(names.length < 2){
        likeText = likeText + 's';
    }

    return likeText + ' this';
  };

  const namesFormatter = (names) => {
    let firstPerson = names.slice(0,1).reduce(
        (resultText, name) =>
            name,
      'No one'
    );
     
    let secondPerson = names.slice(1,2).reduce(
        (resultText, name) =>
            name,
      ''
    );

    let remaining = names.slice(2);

    if (secondPerson.length > 1) {
        if(remaining.length < 1 ){
            secondPerson = ' and ' + secondPerson;
        }else{
            secondPerson = ', ' + secondPerson;
        }
    }

    let remainingPeople = remaining.length  < 2 ? remaining.slice(0) : remaining.length + ' others';
    if(remainingPeople.length > 0) {
        remainingPeople = ' and ' + remainingPeople;
    }


    return `${firstPerson}${secondPerson}${remainingPeople}${likeThis(names)}`;
  };

  module.exports.formatTextFromLikes =  formatTextFromLikes;
